<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
  <div class="container">
    <div class="">

    </div>
    <div class="scrollmenu">
      <span class="logo_helper"></span>
      <a href="Index.php">
        <img
          src="./imagenes/Logo-SNG-bco.png"
          width="90px"
          height="30px"

        />
      </a>
      <a href="Index.php" aria-current="page"
        >Inicio</a>
        <a
          href="registro.php"
          aria-current="page"
          > Registro </a
        >
        <a
          href="verificacion.php"
          aria-current="page"
          >Verificación</a
        >
        <a
          href="seguimiento.php"
          aria-current="page"
          >Seguimiento</a
        >
        <a
          href="reportes.php"
          aria-current="page"
          >Reportes</a
        >
</div>


  <link
    rel="stylesheet"
    href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
    crossorigin="anonymous"
  />

  <link rel="stylesheet" href="./styles/bootstrap.min.css" />

  <link rel="stylesheet" href="./styles/main2.css" />

  <title>SNG-ReconocimientoFacial</title>


  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://sdk.amazonaws.com/js/aws-sdk-2.85.0.min.js"></script>

  <link rel="stylesheet" href="./styles/bootstrap.min.css" />
  <link rel="stylesheet" href="./styles/main.css" />
  <script src="./scripts/GeneradorReportes.js?v=<%= new Random().Next(0,100000) %>"></script>
  <script src="./scripts/generales.js?v=<%= new Random().Next(0,100000) %>"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>


  <title>Reportes</title>
</head>

<body onload="cargarDatos()">
  <div id="oscurecer" style="display:none;">
    <span class="spinner"></span>
  </div>
  <div class="container">
    <div class="form-group" id="id_formulario">
      <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-8 centrar_registro">
          <h2>Reportes</h2>
        </div>
      </div>

      <div class="row margen_seperior">
        <div class="col-md-12">
          <table class="table table-bordered display" id="Table"></table>
        </div>
      </div>

      <div class="row margen_seperior">
        <div class="col-md-2">
          <button name="button" class="form-control btn btn-primary" onclick="descargarExcel()">
            Descargar Excel
          </button>
        </div>
        <div class="col-md-2">
          <button name="button" class="form-control btn btn-primary" onclick="recarga()">
            Recargar
          </button>
        </div>
      </div>

    </div>
  </div>
</body>

</html>
