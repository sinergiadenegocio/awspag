const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const snap = document.getElementById("snap");
const errorMsgElement = document.querySelector('span#errorMsg');

const constraints = {
  audio: true,
  video: {
    width: 1280, height: 720
  }
};

// Access webcam
function startVideo() {
  navigator.getUserMedia({
    video: {}
  },
    stream => video.srcObject = stream,
    err => console.error(err)
  )
}

// Success
function handleSuccess(stream) {
  window.stream = stream;
  video.srcObject = stream;
}

// Load init
startVideo();

// Draw image
var context = canvas.getContext('2d');
snap.addEventListener("click", function () {
  context.drawImage(video, 0, 0, 320, 240);
  var output = document.getElementById("canvas");
  canvas.toBlob(
    function(blob) {
      console.log("Este es el blob: ", blob);
      blob2 = blob;
cargarIDUSR();
    },
    "image/jpeg",
    0.8
  );
});



$(document).ready(function() {

  AWS.config.region = "us-east-1";
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: "us-east-1:fcc097d1-a22b-444e-a42d-f27c00e7744e"
  });

  var s3 = new AWS.S3({
    apiVersion: "2006-03-01",
    params: {
      Bucket: "sngsales-face-scan"
    }
  });

  function guardarBucket(numeroEmpleado) {

    var files = $("#photoupload").prop("files");
    if (!files.length) {
      return alert("Por favor elige un archivo para cargar.");
    }
    //cambiar Nombre
    var nuevoNombre = numeroEmpleado + ".jpg";
    console.log("Archivos "+files[0]);
    var nuevoArchivo = new File([files[0]], nuevoNombre, {
      type: "image/jpg"
    });
    console.log("nuevo archvo "+nuevoArchivo);
    var file = blob2;
    s3.upload(
      {
        Key: nuevoNombre,
        Body: file,
        ACL: "public-read"
      },
      function(err, data) {
        if (err) {
          return alert("Error uploading photo: ", err.message);
        }
        alert("Successfully uploaded photo.");
      }
    );
  }
  function validar() {

    inp_NumeroEmpleado = document.getElementById("inp_NumeroEmpleado").value; //obteniendo el valor que se puso en campo text del formulario
    inp_Nombre = document.getElementById("inp_NumeroEmpleado").value;
    if (inp_NumeroEmpleado.length == 0 || /^\s+$/.test(inp_NumeroEmpleado)) {
      alert("Los campo de texto estan vacio!");
      return false;
    }
    return true;
  }
  $("#btn_Guardar").click(function(event) {
    $('#snap').click();

    //validar datos  si estan llenos
    if (validar()) {
      //enviar por post los datos para guardar en DBa
      var numeroEmpleado = $("#inp_NumeroEmpleado").val();
      var nombre = $("#inp_Nombre").val();
      var apellido = $("#inp_Appellido").val();
      var sueldo = $("#inp_Sueldo").val();

      var sendInfo = {
        numeroEmpleado: new Number(numeroEmpleado),
        nombre: nombre,
        apellido: apellido,
        sueldo: new Number(sueldo)
      };

      var data = JSON.stringify(sendInfo);

      $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "http://localhost:8080/registro/guardar",
        data: data,
        dataType: "json",
        async: true,
        dataFilter: function(data) {
          debugger;
          return data;
        },
        success: function(data) {
          debugger;
          //si se guarda la info correctamente, subir la foto a bucket
          if (data) {
            guardarBucket(numeroEmpleado);
          } else {
            return alert("Ocurrio un problema.");
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          // handle errors
          alert(textStatus);
        }
      });
    } else {
      return false;
    }
  });

  function gethtml(elements) {
    return elements.join("\n");
  }
  $("#photoupload").on("change", function(e) {

    $('#snap').click();

  });
});

function loadFile(event) {
  var output = document.getElementById("canvas");
  output.src = URL.createObjectURL(event.target.files[0]);
}


function cargarIDUSR()
{
  Valores = {

  };
  $.ajax({

    contentType: "application/json; charset=utf-8",
    url:
      "http://localhost:8080/registro/consultarMaximoNumeroEmpleado",

    dataType: "json",
    async: true,

    success: function(data)
    {
      $("#inp_NumeroEmpleado").val(data);

    },
    error: function(
      XMLHttpRequest,
      textStatus,
      errorThrown
    ) {
      $("#inp_NumeroEmpleado").val("87654");
    }
  });
}
