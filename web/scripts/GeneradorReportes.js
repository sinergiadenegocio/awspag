var json;
var cantidad;

function cargarDatos() {
  debugger;
  mostrarSpinner();
  //consumir get ´para crear JSON
  $.ajax({ 
    url: 'http://localhost:8080/seguimiento/consultar',
    success: function(respuesta) {
      debugger;
      cantidad = respuesta.length;
      json = respuesta;

      if (json != "") {
        debugger;
        llenarTabla();
      } else {
        ocultarSpinner();
        return alert("Ocurrió un problema");
      }
    },
    error: function() {
      debugger;
      ocultarSpinner();
      console.log("No se ha podido obtener la información");
    }
  });
}

function llenarTabla() {
  var DatosJson = JSON.parse(JSON.stringify(json));
  $("#Table").append('<thead> <tr><th>Numero Empleado</th>' +
    '<th>Nombre</th>' + '<th>Apellido</th>' + '<th>Sueldo</th>' +
    '<th>Dia</th>' + '<th>Hora</th>' + '<th>Acceso</th></tr></thead><tbody>');
  for (i = 0; i < cantidad; i++) {

    $("#Table").append('<tr>' +
      '<td text-align="center" style="dislay: none;">' + DatosJson[i].numeroEmpleado + '</td>' +
      '<td text-align="center" style="dislay: none;">' + DatosJson[i].nombre + '</td>' +
      '<td text-align="center" style="dislay: none;">' + DatosJson[i].apellido + '</td>' +
      '<td text-align="center" style="dislay: none;">' + DatosJson[i].sueldo + '</td>' +
      '<td text-align="center" style="dislay: none;">' + DatosJson[i].dia + '</td>' +
      '<td text-align="center" style="dislay: none;">' + DatosJson[i].hora + '</td>' +
      '<td text-align="center" style="dislay: none;">' + DatosJson[i].acceso + '</td>' +
      '</tr>');
  }
  $("#Table").append('</tbody>');

  $('#Table').DataTable({
    "iDisplayLength": 2,
  });

  ocultarSpinner();
}

//exportar a excel
function descargarExcel() {
  debugger;
  mostrarSpinner();
  //Creamos un Elemento Temporal en forma de enlace
  var tmpElemento = document.createElement('a');
  // obtenemos la información desde el div que lo contiene en el html
  // Obtenemos la información de la tabla
  var data_type = 'data:application/vnd.ms-excel';
  var tabla_div = document.getElementById('Table');
  var tabla_html = tabla_div.outerHTML.replace(/ /g, '%20');
  tmpElemento.href = data_type + ', ' + tabla_html;
  //Asignamos el nombre a nuestro EXCEL
  tmpElemento.download = 'Reporte.xls';
  // Simulamos el click al elemento creado para descargarlo
  tmpElemento.click();
  ocultarSpinner();
}


function recarga() {
  location.href = location.href
}
//setInterval('recarga()', 20000)
