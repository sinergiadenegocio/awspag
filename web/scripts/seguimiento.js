//convierte en base64 un archivo excel y envia por post el resltado
function handleFileSelect(evt) {
  debugger;
  mostrarSpinner();
  var files = $('#files').prop('files');
  var f = files[0]; // FileList object
  var reader = new FileReader();
  // Closure to capture the file information.
  reader.onload = (function(theFile) {
    return function(e) {
      var binaryData = e.target.result;
      //Converting Binary Data to base 64
      var base64String = window.btoa(binaryData);
      if (base64String != "") {
        var sendInfo = {
          excel64: base64String
        };
        var data = JSON.stringify(sendInfo);

        //mandamos al ´post la base 64
        $.ajax({
          type: "POST",
          contentType: "application/json; charset=utf-8",
          url: "http://localhost:8080/excel/guardarExcel", 
          data: data,
          dataType: "json",
          async: true,
          dataFilter: function(data) {
            debugger;
            return data;
          },
          success: function(data) {
            debugger;
            //si se guarda la info correctamente, subir la foto a bucket
            if (data) {
              document.getElementById('files').value = "";
              ocultarSpinner();
              return alert("Usuarios registrados con éxito.");
            } else {
              ocultarSpinner();
              document.getElementById('files').value = "";
              return alert("Ocurrió un problema.");
            }

          },
          error: function(XMLHttpRequest,
            textStatus,
            errorThrown) {
            // handle errors
            ocultarSpinner();
            alert(textStatus);
          }
        });
      } else {
        ocultarSpinner();
        return alert("Ocurrió un problema.");
      }
    };
  })(f);
  // Read in the image file as a data URL.
  reader.readAsBinaryString(f);
}
