<!DOCTYPE html>
<html lang="es" dir="ltr">



<head>
  <div class="container">
    <div class="">

    </div>
    <div class="scrollmenu">
      <span class="logo_helper"></span>
      <a href="Index.php/">
        <img
          src="./imagenes/Logo-SNG-bco.png"
          width="90px"
          height="30px"

        />
      </a>
      <a href="Index.php" aria-current="page"
        >Inicio</a>
        <a
          href="registro.php"
          aria-current="page"
          > Registro </a
        >
        <a
          href="verificacion.php"
          aria-current="page"
          >Verificación</a
        >
        <a
          href="seguimiento.php"
          aria-current="page"
          >Seguimiento</a
        >
        <a
          href="reportes.php"
          aria-current="page"
          >Reportes</a
        >
</div>


  <link
    rel="stylesheet"
    href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
    integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
    crossorigin="anonymous"
  />

  <link rel="stylesheet" href="./styles/bootstrap.min.css" />

  <link rel="stylesheet" href="./styles/main2.css" />

  <title>SNG-ReconocimientoFacial</title>



  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script defer src="./scripts/fotos.js"></script>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
   <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script src="https://sdk.amazonaws.com/js/aws-sdk-2.85.0.min.js"></script>
   <script src="https://code.jquery.com/jquery-1.10.2.js"></script>


   <link rel="stylesheet" href="./styles/bootstrap.min.css">
   <script src="./scripts/validarCampo.js"></script>
   <link rel="stylesheet" href="./styles/main.css">
   <link rel="stylesheet" href="./styles/main2.css">
   <script src="./scripts/reportes.js"></script>

   <script>
 $( "input" ).focus(function() {
   $( this ).next( "span" ).css( "display", "inline" ).fadeOut( 1000 );
 });
 </script>

  <style>
   span {
     display: none;
   }
   </style>
  <title>Registro</title>
</head>



<body>




     <div class="container-fluid">

       <div class="form-group">

         <div class="row">
           <div class="col-md-4">

           </div>
           <div class="col-md-8 centrar_registro">
             <h2>Registro</h2>
           </div>
         </div>



         <div class="row">



           <div class="col-md-4">
             <div class="row">
               <div class="col-md-12">
                 <img id="imgSalida" class="img-fluid" alt="" />
                 <video id="video" width="300" height="300"  autoplay muted></video>
               </div>
             </div>
           </div>



           <div class="col-md-8">
             <div class="row">
               <div class="col-md-4">
                 <label class="control-label"><strong>Número de Empleado*:</strong></label>
               <p><input type="text" class="form-control" placeholder="Número de empleado" name="inp_NumeroEmpleado" id="inp_NumeroEmpleado" required onKeyPress="return SoloNumeros(event);"  disabled ><span>Número d</span></p>
               </div>



               <div class="col-md-4">
                 <label class="control-label"><strong>Nombre*:</strong></label>
                 <input type="text" class="form-control" maxlength="20" placeholder="Nombre del empleado." id="inp_Nombre" onkeypress="return soloLetras(event);" required onKeyUp="this.value = this.value.toUpperCase();">
               </div>



               <div class="col-md-4">
                 <label class="control-label"><strong>Apellido*:</strong></label>
                 <input type="text" class="form-control" maxlength="20" placeholder="Apellido." id="inp_Appellido" onkeypress="return soloLetras(event);" required onKeyUp="this.value = this.value.toUpperCase();">
               </div>
             </div>



             <div class="row margen_seperior">
               <div class="col-md-4">
                 <label class="control-label"><strong>Sueldo*:</strong></label>
                 <input type="text" class="form-control" maxlength="20" placeholder="Sueldo" id="inp_Sueldo" required onKeyPress="return SoloNumeros(event);">
               </div>



               <div class="col-md-8 ">
                 <label class="control-label"><strong>Key*:</strong></label>
                 <input type="file" class="form-control" accept="image/*" id="photoupload" required  onchange="loadFile(event)">
               </div>
             </div>



             <div class="row margen_seperior float-right">
               <div class="col-md-12">
                 <button name="btnGuardar" class="form-control btn btn-primary" id="btn_Guardar">Guardar</button>
               </div>
             </div>
           </div>
         </div>



       </div>



     </div>




      <!-- Trigger canvas web API -->
      <div class="controller" style="display:none;">
        <button id="snap">Tomar foto</button>
      </div>

      <!-- Webcam video snapshot -->
      <canvas id="canvas" width="320" height="240" style="display:none;"></canvas>

</body>



</html>
