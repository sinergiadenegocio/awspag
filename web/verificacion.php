<!DOCTYPE html>
<html>
  <head>
    <div class="container">
      <div class="">

      </div>
      <div class="scrollmenu">
        <span class="logo_helper"></span>
        <a href="Index.php/">
          <img
            src="./imagenes/Logo-SNG-bco.png"
            width="90px"
            height="30px"

          />
        </a>
        <a href="Index.php" aria-current="page"
          >Inicio</a>
          <a
            href="registro.php"
            aria-current="page"
            > Registro </a
          >
          <a
            href="verificacion.php"
            aria-current="page"
            >Verificación</a
          >
          <a
            href="seguimiento.php"
            aria-current="page"
            >Seguimiento</a
          >
          <a
            href="reportes.php"
            aria-current="page"
            >Reportes</a
          >
</div>


    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
      integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
      crossorigin="anonymous"
    />

    <link rel="stylesheet" href="./styles/bootstrap.min.css" />

    <link rel="stylesheet" href="./styles/main2.css" />

    <title>SNG-ReconocimientoFacial</title>


    <style>
      div.gallery {
        margin: 5px;
        border: 1px solid #ccc;
        float: left;
        width: 200px;
      }

      div.gallery:hover {
        border: 1px solid #777;
      }

      div.gallery img {
        width: 100%;
        height: auto;
      }

      div.desc {
        padding: 10px;
        text-align: center;
        font-size: 12px;
      }

      div.source img {
        width: 200px;
        height: auto;
      }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://sdk.amazonaws.com/js/aws-sdk-2.85.0.min.js"></script>
    <link rel="stylesheet" href="./styles/bootstrap.min.css" />
    <link rel="stylesheet" href="./styles/main.css" />
    <script src="./js/AwsVal.js"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  </head>

  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          
        </div>
        <div class="col-md-8 centrar_registro">
          <h2>Verificaci&oacute;n basada en Rostros</h2>
        </div>
      </div>

      <div class="row pull-center">
        <div class="col-md-1"><br /><br /></div>
        <div class="col-md-4">
          <input id="employeeid" type="text" style="display:none" /> Clave:
          <input id="photoupload" type="file" accept="image/*" />
          <button id="comparephoto" style="display:none">Comparativa</button>
          <br /><br />
          <div class="source"><img id="picture" style="display:none;" /></div>
        </div>
        <div class="col-md-4">
          <!-- Stream video via webcam -->
          <div class="video-wrap">
            <video id="video" playsinline autoplay></video>
          </div>

          <!-- Trigger canvas web API -->
          <div class="controller">
            <button id="snap" style="display:none;">Capture</button>
          </div>

          <!-- Webcam video snapshot -->
          <canvas
            id="canvas"
            width="320"
            height="240"
            style="display:none;"
          ></canvas>
          <script>
            "use strict";

            const video = document.getElementById("video");
            const canvas = document.getElementById("canvas");
            const snap = document.getElementById("snap");
            const errorMsgElement = document.querySelector("span#errorMsg");

            const constraints = {
              audio: false,
              video: {
                width: 320,
                height: 240
              }
            };

            // Access webcam
            async function init() {
              try {
                const stream = await navigator.mediaDevices.getUserMedia(
                  constraints
                );
                handleSuccess(stream);
              } catch (e) {
                errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
              }
            }

            // Success
            function handleSuccess(stream) {
              window.stream = stream;
              video.srcObject = stream;
            }

            // Load init
            init();

            // Draw image
            var context = canvas.getContext("2d");
            snap.addEventListener("click", function() {
              context.drawImage(video, 0, 0, 320, 240);
              canvas.toBlob(
                function(blob) {
                  console.log("Este es el blob: ", blob);
                  blob2 = blob;

                  Usrs.forEach(function(elemento, indice, array) {
                    $(employeeid).val(elemento);
                    console.log("Validando el empleado: ", elemento);
                    $("#comparephoto").click();
                  });
                },
                "image/jpeg",
                0.8
              );
            });

            // here is the most important part because if you dont replace you will get a DOM 18 exception.
          </script>
        </div>
      </div>

      <hr />
      Resultado de Comparativa<br />
      <div id="result"></div>
    </div>
  </body>
</html>
