<!DOCTYPE html>
<html>

<head>
  <style>
    div.gallery {
      margin: 5px;
      border: 1px solid #ccc;
      float: left;
      width: 250px;
    }

    div.gallery:hover {
      border: 1px solid #777;
    }

    div.gallery img {
      width: 100%;
      height: auto;
    }

    div.desc {
      padding: 10px;
      text-align: center;
      font-size: 12px
    }
  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://sdk.amazonaws.com/js/aws-sdk-2.85.0.min.js"></script>
  <script>
    $(document).ready(function() {
      debugger;
      AWS.config.region = 'us-east-1';
      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: 'us-east-1:fcc097d1-a22b-444e-a42d-f27c00e7744e',
      });
      var s3 = new AWS.S3({
        apiVersion: '2006-03-01',
        params: {
          Bucket: 'sngsales-face-scan'
        }
      });
      $('#addphoto').click(function(event) {
        debugger;
        var files = $('#photoupload').prop('files');
        if (!files.length) {
          return alert('Please choose a file to upload!');
        }
        var file = files[0];
        s3.upload({
          Key: file.name,
          Body: file,
          ACL: 'public-read'
        }, function(err, data) {
          if (err) {
            return alert('Error uploading photo: ', err.message);
          }
          alert('Successfully uploaded photo.');
        });
      });

      $('#search').click(function(event) {
        var term = $('#searchtext').val();
        if (!term) {
          return alert('Please enter a search text!');
        }
        var url = 'https://mc5u1fs3h3.execute-api.us-east-1.amazonaws.com/dev/?search=' + term;
        $.get(url, function(data) {
          $('#album-list').empty();
          $.each(data, function(key, value) {
            var imageurl = value.title;
            var tags = value.tags.join(', ');
            var htmlelements = [
              '<div class="gallery">',
              '<a target="_blank" href="' + imageurl + '"><img src="' + imageurl + '"/></a>',
              '<div class="desc">' + tags + '</div>',
              '</div>'
            ]
            var img = gethtml(htmlelements);
            $(img).appendTo('#album-list');
          });
        });
      });

      function gethtml(elements) {
        return elements.join('\n');
      }
    });
  </script>
  <meta charset="utf-8">
</head>

<body bgcolor="#000000" text="#FFFFFF">
  <h1><img src="../../../../../../../Pictures/SNG/logos/SNG blanco sin fondo.png" width="212" height="71" alt="" /> Buscador de Imágenes</h1>
  <input id="photoupload" type="file" accept="image/*">
  <button id="addphoto">AGREGAR FOTO</button>
  <br />
  <hr />
  <input type="text" id="searchtext" placeholder="Search..." />
  <button id="search">BUSCAR</button>
  <br />
  <div id="album-list">
  </div>
</body>

</html>
