var blob2;
var Usrs = ["21491","12345"];
var Contador = 0;


$(document).ready(function() {
  setTimeout(function(){inicializar();},2000);
cargarUSR();

});

function inicializar()
{
  AWS.config.region = "us-east-1";
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: "us-east-1:fcc097d1-a22b-444e-a42d-f27c00e7744e"
  });
  var s3 = new AWS.S3({
    apiVersion: "2006-03-01",
    params: {
      Bucket: "sngsales-face-scan"
    }
  });
  var lambda = new AWS.Lambda({
    apiVersion: "2015-03-31"
  });
  $("#comparephoto").click(function(event) {
    var files = $("#photoupload").prop("files");

    var file = files[0];

    var employeeid = $("#employeeid").val();
    if (employeeid == "") {
      return alert("please enter employee id");
    }
    s3.upload(
      {
        Key: file.name,
        Body: blob2,
        ACL: "public-read"
      },
      function(err, data) {
        if (err) {
          return alert("Error uploading photo: ", err.message);
        } else {
          //successfully uploaded photo
          var lambdaInput = {
            filename: "" + file.name,
            employeeid: employeeid
          };
          var lambdaParams = {
            FunctionName: "sngsales-verify-face-primavera",
            InvocationType: "RequestResponse",
            Payload: JSON.stringify(lambdaInput)
          };
          lambda.invoke(lambdaParams, function(err, data) {
            if (err) {
              return alert("Error invoking lambda: ", err.message);
            } else {
              $("#result").empty();
              if (data != null && data.Payload != null) {
                var output = JSON.parse(data.Payload);
                if (output.result !== null) {
                  var imageurl =
                    "https://s3.amazonaws.com/sngsales-face-scan/" +
                    employeeid +
                    ".jpg";
                  if (output.result == "Green") {
                    Contador++;
                    Valores = {
                      numeroEmpleado: parseInt(employeeid)
                    };
                    $.ajax({
                      type: "POST",
                      contentType: "application/json; charset=utf-8",
                      url:
                        "http://localhost:8080/seguimiento/guardar",
                      data: JSON.stringify(Valores),
                      dataType: "json",
                      async: true,
                      dataFilter: function(data) {
                        return data;
                      },
                      success: function(data) {},
                      error: function(
                        XMLHttpRequest,
                        textStatus,
                        errorThrown
                      ) {
                        alert(textStatus);
                      }
                    });
                  }
                  var htmlelements = [
                    "<div><b>" + "Registros detectados" + "</b></div>",
                    "<div><b>" + Contador + "</b></div>"


                  ];
                  var img = gethtml(htmlelements);
                  $(img).appendTo("#result");
                }
              }
            }
          });
        }
      }
    );
  });


  $("#photoupload").on("change", function(e) {
    $("#picture").attr("src", URL.createObjectURL(e.target.files[0]));
    $("#result").empty();
    $("#photoupload").hide();
    $('#snap').hide();
    setInterval("$('#snap').click();", 5000);

  });
}
function cargarUSR()
{
  Valores = {

  };
  $.ajax({

    contentType: "application/json; charset=utf-8",
    url:
      "http://localhost:8080/registro/consultarNumeroEmpleado",

    dataType: "json",
    async: true,

    success: function(data)
    {
      Usrs=data;

    },
    error: function(
      XMLHttpRequest,
      textStatus,
      errorThrown
    ) {
      alert(textStatus);
    }
  });
}

function gethtml(elements) {
  return elements.join("\n");
}
